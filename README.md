# mobiliscope-services
A collection of docker configurations to build up the whole Mobiliscope ecosystem

# Prerequisites
## Installation
- git
- docker
- docker-compose

Then run:
```
// Docker stuff preparation
sudo chmod 777 /var/run/docker.sock
docker network create proxy

// Clone the repository
git clone https://gitlab.huma-num.fr/mobiliscope/services.git
```

Add a .env file at the root project with the following lines (fill in the blanks):
```
MY_DOMAIN=
DEFAULT_NETWORK=proxy
MINIO_ADMIN=
MINIO_ADMIN_PASS=
```

### Use Traefik (reverse proxy) and mobiliscope services

#### Up and down services
> ssh mobiliscope@mobiliscope.huma-num.fr  // connect to the huma-num machine
> 
> mobiliscope@mobiliscope> cd services // go to the mobiliscope services directory (git one)
> 
> mobiliscope@mobiliscope> docker-compose up -d // starts all services
>
> mobiliscope@mobiliscope> docker-compose up -d mobiliquest // (re)starts only mobiliquest service
> 
> mobiliscope@mobiliscope> docker-compose down // stops all services
>
> mobiliscope@mobiliscope> docker-compose stop mobiliquest // stops only mobiliquest service
> 
> mobiliscope@mobiliscope> docker ps //check started service statuses

#### Re-build services
> mobiliscope@mobiliscope> docker-compose build --no-cache mobiliquest // Rebuild mobiliquest after a repository push
>
> mobiliscope@mobiliscope> docker-compose build --no-cache mobiliscope // Rebuild mobiliscope after a repository push

#### Debug
> mobiliscope@mobiliscope> docker logs reverse-proxy // display logs of the traefik reverse-proxy
>

## Services
### Check services
Browse traefik.ajmr.mobiliscope.com

### Minio web interface
Browse minioadmin.ajmr.mobiliscope.com

### Minio API
minio.ajmr.mobiliscope.com

### Mobiliscope
Browse ajmr.mobiliscope.com
